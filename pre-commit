#!/usr/bin/env python3
"""
Pre-commit
Code inspired by https://gist.github.com/810399
"""
# from __future__ import with_statement, print_function
import re as regex
import sys
import subprocess

COLOR_FAIL = '\033[91m'
COLOR_INFO = '\033[94m'
COLOR_SUCCESS = '\033[92m'
COLOR_NEUTRAL = '\033[0m'

# Fill one of both SELECT_CODES or IGNORE_CODES. If not, an error will be raised
SELECT_CODES = []
IGNORE_CODES = ["E121", "E122", "E123", "E124", "E125",
                "E126", "E127", "E128", "E129", "E131"]

OVERRIDES = ["--max-line-length=120"]


def main():
    files_modified = get_modified_files()
    pep8_errors = run_pep8_checker(files_modified)
    cyclomatic_complexity_errors = run_cc_checker(files_modified)
    maintainability_index_errors = run_mi_checker(files_modified)
    test_errors = run_tests()
    errors = pep8_errors or cyclomatic_complexity_errors or maintainability_index_errors or test_errors

    if errors:
        print(COLOR_FAIL,
              u'\nSome problems were detected. Please fix them or',
              u'force the commit with "git commit --no-verify".',
              COLOR_NEUTRAL)
        sys.exit(1)

    print(COLOR_SUCCESS,
          '\n##', u"\U0001F37B ", 'Commit can be done!', u"\U0001F37B ", '##',
          COLOR_NEUTRAL)


def get_modified_files():
    regex_py_modified_files = regex.compile('^[AM]+\s+(?P<name>.*\.py$)', regex.MULTILINE)

    files_changed_process = subprocess.Popen(('git', 'status', '--porcelain'),
                                             stdout=subprocess.PIPE,
                                             stderr=subprocess.PIPE)
    out, error = files_changed_process.communicate()

    return regex_py_modified_files.findall(out.decode("utf-8"))


def run_pep8_checker(files_modified):
    print(COLOR_INFO,
          '\nChecking that code is pep8 compliant...',
          COLOR_NEUTRAL)
    pep8_command = pep8_compose_command()

    for file_modified in files_modified:
        pep8_command.append(file_modified)

    pep8_command_process = subprocess.Popen(tuple(pep8_command),
                                            stdout=subprocess.PIPE,
                                            stderr=subprocess.PIPE)

    output_pep8, error = pep8_command_process.communicate()

    if not output_pep8:
        print(COLOR_SUCCESS,
              u'\nNo PEP8 style violations detected!\n',
              COLOR_NEUTRAL)
        return False
    print(COLOR_FAIL,
          '\nErrors on pep8 standars were found:',
          COLOR_NEUTRAL)
    errors_found = output_pep8.decode("utf-8")
    print(errors_found)

    return errors_found


def pep8_compose_command():
    if SELECT_CODES and IGNORE_CODES:
        print(u'Error: pycodestyle options => select and ignore codes are mutually exclusive')
        sys.exit(1)
    command = ['pycodestyle']
    if SELECT_CODES:
        command.extend(('--select', ','.join(SELECT_CODES)))
    if IGNORE_CODES:
        command.extend(('--ignore', ','.join(IGNORE_CODES)))
    command.extend(OVERRIDES)
    return command


def run_cc_checker(files_modified):
    cc_command = ['radon', 'cc', '--min=C', '-s']

    for file_modified in files_modified:
        cc_command.append(file_modified)

    print(COLOR_INFO,
          '\nChecking that cyclomatic complexity is under threshold...',
          COLOR_NEUTRAL)

    cc_command_process = subprocess.Popen(tuple(cc_command),
                                          stdout=subprocess.PIPE,
                                          stderr=subprocess.PIPE)

    output_cc, error = cc_command_process.communicate()

    if not output_cc:
        print(COLOR_SUCCESS,
              u'\nNo cyclomatic complexity alerts!\n',
              COLOR_NEUTRAL)
        return False
    print(COLOR_FAIL,
          '\nCyclomatic complexity alerts:',
          COLOR_NEUTRAL)
    errors_found = output_cc.decode("utf-8")
    print(errors_found)

    return errors_found


def run_mi_checker(files_modified):
    mi_command = ['radon', 'mi', '--min=B', '-s']

    for file_modified in files_modified:
        mi_command.append(file_modified)

    print(COLOR_INFO,
          '\nChecking that maintainability index has not any alert...',
          COLOR_NEUTRAL)

    mi_command_process = subprocess.Popen(tuple(mi_command),
                                          stdout=subprocess.PIPE,
                                          stderr=subprocess.PIPE)

    output_mi, error = mi_command_process.communicate()

    if not output_mi:
        print(COLOR_SUCCESS,
              u'\nNo maintainability index alerts!\n',
              COLOR_NEUTRAL)
        return False
    print(COLOR_FAIL,
          '\nMaintainability index alerts:',
          COLOR_NEUTRAL)
    errors_found = output_mi.decode("utf-8")
    print(errors_found)

    return errors_found


def run_tests():
    # Running tests with pytest
    # Check pytest documentation at -> https://docs.pytest.org/en/latest/usage.html

    print(COLOR_INFO,
          '\nRunning tests...',
          COLOR_NEUTRAL)

    pytest_process = subprocess.Popen(('python', '-m', 'pytest', '-q', '--color=yes'),
                                      stdout=subprocess.PIPE,
                                      stderr=subprocess.PIPE)

    output_test, error = pytest_process.communicate()

    errors_found = output_test.decode("utf-8")
    print(errors_found)

    if pytest_process.returncode in (0, 5):
        return False

    return errors_found


if __name__ == '__main__':
    main()
