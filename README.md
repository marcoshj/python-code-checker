# Python code checker
This repository contains different tools/scripts 
that will help us write better code.

## Requirements
* Pycodestyle (pep8 code compliance)
* Radon (cyclomatic complexity and maintainability index)
* Pytest (or django-pytest in case your using django framework)

```
pip install pycodestyle
pip install radon
pip install pytest
```
